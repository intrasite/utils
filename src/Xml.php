<?php

namespace Intrasite\Utils;

class Xml
{
    /**
     * Applies a xsl stylesheet, converting source xml to other xml.
     *
     * @param string $xmlString
     * @param string $xslFilePath
     *
     * @return SimpleXMLElement
     */
    public static function convertWithXsl($xmlString, $xslFilePath)
    {
        // TODO: clean the root tag from xsl namespaces?
        $source = $xmlString;
        //$source = str_replace(' xmlns="http://www.vjoon.com/K4Export/2.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" format="intermediate" version="2.2" xsi:schemaLocation="http://www.vjoon.com/K4Export/2.2 K4Export_2_2.xsd"', '', $source);
        //$source = str_replace(' xmlns="http://www.vjoon.com/K4Export/2.4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" format="intermediate" version="2.4" xsi:schemaLocation="http://www.vjoon.com/K4Export/2.4 K4Export_2_4.xsd"', '', $source);
        //$source = str_replace(' xmlns="http://www.vjoon.com/K4Export/2.4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" format="archive" version="2.4" xsi:schemaLocation="http://www.vjoon.com/K4Export/2.4 K4Export_2_4.xsd"', '', $source);

        $xmlSource = self::loadXml($source);

        // convert
        $xslDoc = new DOMDocument();
        $xslDoc->load($xslFilePath);

        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($xmlSource->asXML());

        $proc = new XSLTProcessor();
        $proc->importStylesheet($xslDoc);
        $output = $proc->transformToXML($xmlDoc);

        $xmlOutput = self::loadXml($output);

        return $xmlOutput;
    }

    /**
     * Converts a plain string into an xml object.
     *
     * @param string $content
     *
     * @return SimpleXMLElement
     */
    public static function loadXml($content)
    {
        return simplexml_load_string($content);
    }
}
