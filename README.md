# Skeleton PHP package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/intrasite/utils.svg?style=flat-square)](https://packagist.org/packages/intrasite/utils)
[![Total Downloads](https://img.shields.io/packagist/dt/intrasite/utils.svg?style=flat-square)](https://packagist.org/packages/intrasite/utils)


This is where your description should go. Try and limit it to a paragraph or two. Consider adding a small example.

## Installation

You can install the package via composer:

```bash
composer require intrasite/utils
```

## Usage

``` php
$xml = new Intrasite\Utils\Xml();
```

## Changelog

Please see [CHANGELOG](https://bitbucket.org/intrasite/utils/src/master/CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://bitbucket.org/intrasite/utils/src/master/CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email tomas.bracke@telenet.be instead of using the issue tracker.

## Credits

- [Tomas Bracke](https://intrasite.be)

## License

The MIT License (MIT). Please see [License File](https://bitbucket.org/intrasite/utils/src/master/LICENSE.md) for more information.
