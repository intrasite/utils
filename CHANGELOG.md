# Changelog

All notable changes to `utils` will be documented in this file

## 1.0.2 - 2020-09-05

- auto update test

## 1.0.1 - 2020-09-04

- added first 2 methods

## 1.0.0 - 2020-09-04

- initial release
